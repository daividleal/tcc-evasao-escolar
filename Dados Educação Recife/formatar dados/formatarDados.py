import string
arquivo = open("baseFinal.csv","r+")
resultados = arquivo.readlines()
arquivo.close()

itens = resultados[0].split(",")
qtdItens = len(itens)
linhas = [itens]
colunas = []

inserts = ["m","b","b","b","b","m","b","b","b","b","b","m","m","m","b","b","m","b","b","b","b"]

for i in range(0, qtdItens):
    column = []
    elementoFaltando = False
    for j in range(1, len(resultados)):
        if(resultados[j].split(",")[i] == ""):
            elementoFaltando = True
        column.append(resultados[j].split(",")[i])

    if(elementoFaltando):
        resp = inserts.pop(0)
        if(resp.lower() == "m"):
            count = 0
            soma = 0
            for x in column:
                if(x == ""):
                    pass                   
                elif(itens[-1] == itens[i]):
                    pass
                else:
                    count += 1
                    soma += int(x)

            media = soma/count
            media = int(media)

            for x in range(0, len(column)):
                if(column[x] == ""):
                   column[x] = str(media)
        elif(resp.lower() == "b"):
            for x in range(0, len(column)):
                if(column[x] == ""):
                   column[x] = "0"

    colunas.append(column)

tabelaFinal = []

for i in range(1,len(resultados)):
    linhas.append([])

for i in range(1, len(linhas)):
    for x in colunas:
        if(i != 0):
            linhas[i].append(x[i-1])

for i in linhas:
    tabelaFinal.append(string.join(i, ","))

nomeArquivo = "baseFinalFormatada.csv"
try:
    arquivo = open(nomeArquivo, 'r+')
except Exception:
    arquivo = open(nomeArquivo, 'w+')

arquivo.writelines(tabelaFinal)
arquivo.close()
