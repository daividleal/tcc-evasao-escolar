def readAllDataByYear(years):
    todasAsEscolasComRepeticoes = {}
    for y in years:
        todasAsEscolasComRepeticoes[y] = []
        arquivo = open("escolascomdesistentes"+str(y)+".csv", "r+")
        for i in arquivo.readlines():
            todasAsEscolasComRepeticoes[y].append(i)
        arquivo.close()
    return todasAsEscolasComRepeticoes

def retirarRepeticoes(escolasCRByYear):
    years = escolasCRByYear.keys()
    years.sort()
    semRepeticoes = escolasCRByYear[years[-1]][:] #clone da lista com as escolas do �ltimo ano
    
    for i in range(0,len(years)-1): #varre as outras listas de escolas procurando as que n�o s�o do �ltimo ano
        for j in escolasCRByYear[years[i]]:
            add = True
            for k in semRepeticoes:
                ## para os arquivos escolascomdesistentes2000(2011,2012,2014) e 2015
                ## tratar como int
                if(k.split(";")[1] != "codigo_escola" and j.split(";")[1] != "codigo_escola"):
                    if(int(k.split(";")[1]) == int(j.split(";")[1])):
                        add = False
                        break
            if(add):
                semRepeticoes.append(j)
    return semRepeticoes
    
nomeArquivo = "unified_2011_2012_2014_2015.csv"
try:
    arquivo = open(nomeArquivo, 'r+')
except Exception:
    arquivo = open(nomeArquivo, 'w+')

arquivo.writelines(retirarRepeticoes(readAllDataByYear([2000,2015])))
arquivo.close()
