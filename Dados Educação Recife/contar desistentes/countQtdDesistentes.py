def readPlanilha(year):        
    listaEscolasQtdDesistentes = {}
        
    arquivo = open("resultadosFinais"+str(year)+".csv", "r+")
    resultados = arquivo.readlines()
    arquivo.close()
    dic = {}
    for i in resultados:
        linha = i.split(";") 
        if(dic.has_key(linha[1])):
            if(linha[-2] == "D"):
                total = dic[linha[1]][0]+1
                qtd = dic[linha[1]][1]+1
                dic[linha[1]] = (total, qtd)
            else:
                total = dic[linha[1]][0]+1
                qtd = dic[linha[1]][1]
                dic[linha[1]] = (total, qtd)
        else:
            dic[linha[1]] = (0,0)
    return dic

def isEscolaOnLista(escolaNome, lista, indexEscolaNome):
    result = False
    for i in lista:
        linha = i.split(";")
        if(linha[indexEscolaNome] == escolaNome):
            result = True
            break
    return result

def generatePlanilhas(year):
    arquivo = open("escolas" + str(year) +".csv", "r+")
    escolas = arquivo.readlines()
    arquivo.close()
    novaPlanilha = [escolas[0][0:-1] + ";qtdDesistentes \n"]

    escolaQtdDesistentes = readPlanilha(year)
    indexNome = 2
    if(2015):
        indexNome = 5
    for i in escolaQtdDesistentes:
        contain = False
        for j in escolas:
            escolaNome = j.split(";")[indexNome]
            if(i != "escola" and escolaNome.lower().find(i.lower()) != -1 and (not isEscolaOnLista(escolaNome, novaPlanilha, indexNome))):
                if(j.lower().find("\n") != -1):
                    a = j[0:-1]
                    a += (";" + str(escolaQtdDesistentes[i])) + "\n"
                    novaPlanilha.append(a)
                else:
                    j += (";" + str(escolaQtdDesistentes[i])) + "\n"
                    novaPlanilha.append(j)
                contain = True
                break

    nomeArquivo = "escolascomdesistentes"+str(year)+".csv"
    try:
        arquivo = open(nomeArquivo, 'r+')
    except Exception:
        arquivo = open(nomeArquivo, 'w+')

    arquivo.writelines(novaPlanilha)
    arquivo.close()


years = [2011, 2012, 2013, 2014, 2015, 2016]
for i in years:
    generatePlanilhas(str(i))
