from sklearn.ensemble import AdaBoostRegressor, BaggingRegressor, ExtraTreesRegressor, RandomForestRegressor, GradientBoostingRegressor
from sklearn.model_selection import RandomizedSearchCV, StratifiedKFold, KFold
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.optimizers import SGD
from keras.wrappers.scikit_learn import KerasRegressor
from keras.constraints import maxnorm
from sklearn.svm import SVR, NuSVR
import numpy as np
import matplotlib.pyplot as plt
from mlxtend.feature_selection import SequentialFeatureSelector as SFS
from mlxtend.plotting import plot_sequential_feature_selection as plot_sfs
import matplotlib.pyplot as plt
from sklearn.model_selection import GridSearchCV
from mlxtend.plotting import plot_sequential_feature_selection as plot_sfs

""" Read Data data """
arquivo = open("Au.txt", "r+")
texto = arquivo.readlines()
arquivo.close()
a = []
x = []
y = []

for i in texto:
    a.append(i.split(","))

for i in range(1,len(a)):
    x.append([])
    for j in range(0,len(a[i])):
        if(65 == j):
            y.append(float(a[i][j]))
        else:
            x[i-1].append(float(a[i][j]))
            
def createModel(neurons=17, activation='relu', init_mode='uniform', learn_rate=0.0012264789207924204, momentum=0.89659298182794145, dropout_rate=0.018702540495271529, weight_constraint=3, input_dim=65):
        """ Building network """
        model = Sequential()
        model.add(Dense(neurons, kernel_initializer=init_mode, activation=activation, input_dim=input_dim, kernel_constraint=maxnorm(weight_constraint)))
        model.add(Dropout(float(dropout_rate)))
        model.add(Dense(1, kernel_initializer=init_mode, activation='linear'))

        """ Training """
        sgd = SGD(lr=float(learn_rate), decay=1e-6, momentum=float(momentum), nesterov=True)
        model.compile(loss='mean_absolute_error',
                    optimizer=sgd,
                    metrics=['mae'])
        return model


X_test = x[330:]
Y_test = y[330:]
X_train = x[0:330] 
Y_train = y[0:330]

""" Trained models """
svr = SVR()
svr.C = 0.2898087068229081
svr.degree = 4
svr.epsilon = 1.4006088388851361
svr.gamma = 0.11925439566117002
svr.kernel = 'linear'

nu_svr = NuSVR()
nu_svr.C = 3.7132193884820839
nu_svr.degree = 5
nu_svr.nu = 0.91892888835741759
nu_svr.gamma = 0.034429704202198173
nu_svr.kernel = 'sigmoid'

model = KerasRegressor(build_fn=createModel, epochs=392, batch_size=69, verbose=0)

estimators = []
estimators.append(AdaBoostRegressor(svr))
estimators.append(BaggingRegressor(svr))

estimators.append(AdaBoostRegressor(nu_svr))
estimators.append(BaggingRegressor(nu_svr))

estimators.append(AdaBoostRegressor(model))
estimators.append(BaggingRegressor(model))

##estimators.append(AdaBoostRegressor())
##estimators.append(BaggingRegressor())
##estimators.append(ExtraTreesRegressor())
##estimators.append(RandomForestRegressor())
##estimators.append(GradientBoostingRegressor())

##grid = GridSearchCV(AdaBoostRegressor(model), dict(), cv=10, refit=True, scoring='neg_mean_absolute_error')
##grid.fit(X_train,Y_train)
##print(grid.grid_scores_)
##plt.scatter(Y_test, grid.predict(X_test), c="g", alpha=0.5,  label="Luck")
##plt.ylabel("Valores preditos")
##plt.xlabel("Valores Reais")
##plt.title("Resultados do ExtraTreesRegressor")
##plt.grid()
##plt.show()

labels = [
          'AdaBoostRegressor-ESVR', 'BaggingRegressor-ESVR',
          'AdaBoostRegressor-NUSVR', 'BaggingRegressor-NUSVR',
          'AdaBoostRegressor-MLP', 'BaggingRegressor-MLP']
##          ,
##          'AdaBoostRegressor', 'BaggingRegressor','ExtraTreesRegressor', 'RandomForestRegressor', 'GradientBoostingRegressor']
for clf, label in zip(estimators, labels):
	grid = GridSearchCV(clf, dict(), cv=10, refit=True, scoring='neg_mean_absolute_error')
##	grid.fit(X_train,Y_train)
	grid.fit(x,y)
	print(grid.grid_scores_)
##	plt.scatter(Y_test, grid.predict(X_test), c="g", alpha=0.5,  label="Luck")
	plt.scatter(y, grid.predict(x), c="g", alpha=0.5,  label="Luck")
	plt.ylabel("Valores preditos")
	plt.xlabel("Valores Reais")
	plt.title("Resultados do " + label)
	plt.grid()
	plt.show()
