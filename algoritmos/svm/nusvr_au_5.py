from sklearn.preprocessing import minmax_scale, scale
from sklearn.model_selection import RandomizedSearchCV
from sklearn.svm import NuSVR
from scipy.stats import expon, uniform
from numpy import loadtxt
import numpy as np
	
""" fix random seed for reproducibility """
seed = 5
np.random.seed(seed)

""" Read Data data """
arquivo = open("Au.txt", "r+")
texto = arquivo.readlines()
arquivo.close()
a = []
x = []
y = []

for i in texto:
    a.append(i.split(","))

for i in range(1,len(a)):
    x.append([])
    for j in range(0,len(a[i])):
        if(65 == j):
            y.append(float(a[i][j]))
        else:
            x[i-1].append(float(a[i][j]))

##""" Pre-processing data """
##x=loadtxt('Au.txt')
##y = x[:,-1]
##x = x[:,0:-1]
minmax_scale(x,copy=False)

""" Training model """
svr = NuSVR()
param_dist = {'C': expon(scale=100,loc=1e-10), 'nu': uniform(loc=1e-10,scale=1), 'degree': [2,3,4,5], 
        'gamma': expon(scale=1,loc=1e-10), 'kernel': ['linear','poly','rbf','sigmoid']}
scoring = {'mse': 'neg_mean_squared_error', 'mae': 'neg_mean_absolute_error', 'r2':'r2', 'ev':'explained_variance'}
n_iter_search=100
grid = RandomizedSearchCV(svr, param_distributions=param_dist, n_iter=n_iter_search, cv=10, verbose=1, n_jobs=1, scoring=scoring, refit='mae')
grid_result = grid.fit(x, y)

""" summarize results """
a = []
a.append("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_) + "\n")
a.append("----------------------------------------------------------" + "\n")
means_mae = grid_result.cv_results_['mean_test_mae']
stds_mae = grid_result.cv_results_['std_test_mae']
means_mse = grid_result.cv_results_['mean_test_mse']
stds_mse = grid_result.cv_results_['std_test_mse']
means_r2 = grid_result.cv_results_['mean_test_r2']
stds_r2 = grid_result.cv_results_['std_test_r2']
means_ev = grid_result.cv_results_['mean_test_ev']
stds_ev = grid_result.cv_results_['std_test_ev']
params = grid_result.cv_results_['params']
for mean_mae, stdev_mae, mean_mse, stdev_mse, mean_r2, stdev_r2, mean_ev, stdev_ev, param in zip(means_mae, stds_mae,means_mse, stds_mse, means_r2, stds_r2,means_ev, stds_ev,params):
    a.append("mean mae=%f (std=%f) with: %r" % (mean_mae, stdev_mae, param) + "\n")
    a.append("mean mse=%f (std=%f) with: %r" % (mean_mse, stdev_mse, param) + "\n")
    a.append("mean r2=%f (std=%f) with: %r" % (mean_r2, stdev_r2, param) + "\n")
    a.append("mean ev=%f (std=%f) with: %r" % (mean_ev, stdev_ev, param) + "\n")
    a.append("****************************************************" + "\n")

arquivo = open("restauldo_nu_svr_5.txt", "w+")
arquivo.writelines(a)
arquivo.close()
#grid.best_estimator_.model.save('results/results_svr_au.h5')
