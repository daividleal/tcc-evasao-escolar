from sklearn.model_selection import KFold
from sklearn.svm import NuSVR
import numpy as np
from sklearn.feature_selection import RFECV
import matplotlib.pyplot as plt
from mlxtend.plotting import plot_sequential_feature_selection as plot_sfs
from sklearn import linear_model, decomposition, datasets
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
import random

""" Read Data data """
arquivo = open("Au.txt", "r+")
texto = arquivo.readlines()
arquivo.close()
a = []
x = []
y = []

for i in texto:
    a.append(i.split(","))

for i in range(1,len(a)):
    x.append([])
    for j in range(0,len(a[i])):
        if(65 == j):
            y.append(float(a[i][j]))
        else:
            x[i-1].append(float(a[i][j]))

""" Trained model """
nu_svr = NuSVR()
nu_svr.C = 3.7132193884820839
nu_svr.degree = 5
nu_svr.nu = 0.91892888835741759
nu_svr.gamma = 0.034429704202198173
nu_svr.kernel = 'sigmoid'

pca = decomposition.PCA()
pipe = Pipeline(steps=[('pca', pca), ('clf', nu_svr)])

# Parameters of pipelines can be set using ‘__’ separated parameter names:
estimator = GridSearchCV(pipe,dict(pca__n_components=[42]),scoring='neg_mean_absolute_error', cv=KFold(10))
estimator.fit(x, y)

plt.scatter(y, estimator.predict(x), c="g", alpha=0.5,  label="Luck")
plt.ylabel("Valores preditos pela NU_SVR")
plt.xlabel("Valores Reais")
plt.title('Resultados da SVM com o uso do PCA')
plt.show()