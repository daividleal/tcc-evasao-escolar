from sklearn.preprocessing import minmax_scale, scale
from sklearn.model_selection import KFold
from sklearn.svm import SVR
import numpy as np
import matplotlib.pyplot as plt
from mlxtend.feature_selection import SequentialFeatureSelector as SFS
from mlxtend.plotting import plot_sequential_feature_selection as plot_sfs
from sklearn import linear_model, decomposition, datasets
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
import random

""" Read Data data """
arquivo = open("Au.txt", "r+")
texto = arquivo.readlines()
arquivo.close()
a = []
x = []
y = []

for i in texto:
    a.append(i.split(","))

for i in range(1,len(a)):
    x.append([])
    for j in range(0,len(a[i])):
        if(65 == j):
            y.append(float(a[i][j]))
        else:
            x[i-1].append(float(a[i][j]))

""" Trained model """
svr = SVR()
svr.C = 0.2898087068229081
svr.degree = 4
svr.epsilon = 1.4006088388851361
svr.gamma = 0.11925439566117002
svr.kernel = 'linear'
pca = decomposition.PCA()
pipe = Pipeline(steps=[('pca', pca), ('svr', svr)])

# Parameters of pipelines can be set using ‘__’ separated parameter names:
estimator = GridSearchCV(pipe,dict(pca__n_components=[43]),scoring='neg_mean_absolute_error', cv=KFold(10))
estimator.fit(x, y)

plt.scatter(y, estimator.predict(x), c="g", alpha=0.5,  label="Luck")
plt.ylabel("Valores preditos pela SVR")
plt.xlabel("Valores Reais")
plt.title('Resultados da SVM com o uso do PCA')
plt.show()
