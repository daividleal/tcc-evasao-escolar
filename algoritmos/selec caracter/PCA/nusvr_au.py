from sklearn.model_selection import KFold
from sklearn.svm import NuSVR
import numpy as np
from sklearn.feature_selection import RFECV
import matplotlib.pyplot as plt
from mlxtend.plotting import plot_sequential_feature_selection as plot_sfs
from sklearn import linear_model, decomposition, datasets
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
import random

""" Read Data data """
arquivo = open("Au.txt", "r+")
texto = arquivo.readlines()
arquivo.close()
a = []
x = []
y = []

for i in texto:
    a.append(i.split(","))

for i in range(1,len(a)):
    x.append([])
    for j in range(0,len(a[i])):
        if(65 == j):
            y.append(float(a[i][j]))
        else:
            x[i-1].append(float(a[i][j]))

""" Trained model """
nu_svr = NuSVR()
nu_svr.C = 3.7132193884820839
nu_svr.degree = 5
nu_svr.nu = 0.91892888835741759
nu_svr.gamma = 0.034429704202198173
nu_svr.kernel = 'sigmoid'

pca = decomposition.PCA()
pipe = Pipeline(steps=[('pca', pca), ('clf', nu_svr)])

# Prediction
n_components = []
for i in range(1,66):
	n_components.append(i)
# Parameters of pipelines can be set using ‘__’ separated parameter names:
estimator = GridSearchCV(pipe,dict(pca__n_components=n_components),scoring='neg_mean_absolute_error', cv=KFold(10))
estimator.fit(x, y)
results = []
for i in estimator.grid_scores_:
    results.append(i[1]);
plt.plot(n_components, results, "bo", n_components, results, "k")
plt.ylim([(estimator.best_score_ - 1), (estimator.best_score_ + 1)])
plt.ylabel("Cross Validation Score (MAE)")
plt.xlabel("Number of features selected")
plt.title('Resultados PCA')
plt.axvline(estimator.best_estimator_.named_steps['pca'].n_components,
            linestyle=':', label='Melhor Resultado (MAE)')
plt.legend(prop=dict(size=12))
plt.grid()
plt.show()
