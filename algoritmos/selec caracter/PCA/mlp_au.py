import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='8'
from sklearn.model_selection import KFold
import scipy
from numpy import loadtxt
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.optimizers import SGD
from keras.wrappers.scikit_learn import KerasRegressor
from keras.constraints import maxnorm
from sklearn.externals import joblib
from mlxtend.plotting import plot_sequential_feature_selection as plot_sfs
from sklearn import linear_model, decomposition, datasets
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
import matplotlib.pyplot as plt
import random

""" fix random seed for reproducibility """
seed = 19
np.random.seed(seed)

""" Read Data data """
arquivo = open("Au.txt", "r+")
texto = arquivo.readlines()
arquivo.close()
a = []
x = []
y = []
x1 = []

for i in texto:
    a.append(i.split(","))

for i in range(1,len(a)):
    x.append([])
    for j in range(0,len(a[i])):
        if(65 == j):
            y.append(float(a[i][j]))
        else:
            x[i-1].append(float(a[i][j]))

count = 0
for i in x:
    x1.append([])
    for j in i:
        x1[count].append(float(str(round(j,2))))
    count+=1

""" Create model """
results = []
n_components = []
for i in range(1,66):
	n_components.append(i)
n_components.sort()
best_score = -1000
print(n_components)
for i in n_components:
    def createModel(neurons=17, activation='relu', init_mode='uniform', learn_rate=0.0012264789207924204, momentum=0.89659298182794145, dropout_rate=0.018702540495271529, weight_constraint=3, input_dim=i):
        """ Building network """
        model = Sequential()
        model.add(Dense(neurons, kernel_initializer=init_mode, activation=activation, input_dim=input_dim, kernel_constraint=maxnorm(weight_constraint)))
        model.add(Dropout(float(dropout_rate)))
        model.add(Dense(1, kernel_initializer=init_mode, activation='linear'))

        """ Training """
        sgd = SGD(lr=float(learn_rate), decay=1e-6, momentum=float(momentum), nesterov=True)
        model.compile(loss='mean_absolute_error',
                    optimizer=sgd,
                    metrics=['mae'])
        return model

    model = KerasRegressor(build_fn=createModel, epochs=392, batch_size=69, verbose=0)
    pca = decomposition.PCA()
    pipe = Pipeline(steps=[('pca', pca), ('clf', model)])
    # Prediction
    # Parameters of pipelines can be set using ‘__’ separated parameter names:
    estimator = GridSearchCV(pipe,dict(pca__n_components=[i]),scoring='neg_mean_absolute_error', cv=KFold(10))
    estimator.fit(x, y)
    result =  estimator.grid_scores_[0][1]
    if(best_score < result):
        best_score = result
    results.append(result)

plt.plot(n_components, results, "bo", n_components, results, "k")
plt.ylim([(best_score - 1), (best_score + 1)])
plt.ylabel("Cross Validation Score (MAE)")
plt.xlabel("Number of features selected")
plt.title('Resultados PCA')
n_components_best = 0
for i in range(0, len(results)):
    if(results[i] == best_score):
        n_components_best = n_components[i]
        
plt.axvline(n_components_best, linestyle=':', label='Número de componentes Escolhidos')
plt.legend(prop=dict(size=12))
plt.grid()
plt.show()
