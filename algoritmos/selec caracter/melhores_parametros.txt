svr_au.py
Best: -4.676283 using {'C': 0.2898087068229081, 'degree': 4, 'epsilon': 1.4006088388851361, 'gamma': 0.11925439566117002, 'kernel': 'linear'}
mean mae=-4.676283 (std=1.168069) with: {'C': 0.2898087068229081, 'degree': 4, 'epsilon': 1.4006088388851361, 'gamma': 0.11925439566117002, 'kernel': 'linear'}

nu_svr.py
Best: -4.648690 using {'C': 3.7132193884820839, 'degree': 5, 'nu': 0.91892888835741759, 'gamma': 0.034429704202198173,  'kernel': 'sigmoid'}
mean mae=-4.648690 (std=1.293212) with: {'kernel': 'sigmoid', 'C': 3.7132193884820839, 'gamma': 0.034429704202198173, 'degree': 5, 'nu': 0.91892888835741759}

mlp.pys
Best: -4.462528 using {'activation': 'relu', 'batch_size': 69, 'dropout_rate': 0.018702540495271529, 'epochs': 392, 'init_mode': 'uniform', 'learn_rate': 0.0012264789207924204, 'momentum': 0.89659298182794145, 'neurons': 17, 'weight_constraint': 3}
mean mae=-4.462528 (std=1.242565) with: {'activation': 'relu', 'batch_size': 69, 'dropout_rate': 0.018702540495271529, 'epochs': 392, 'init_mode': 'uniform', 'learn_rate': 0.0012264789207924204, 'momentum': 0.89659298182794145, 'neurons': 17, 'weight_constraint': 3}