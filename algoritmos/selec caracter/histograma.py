import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
""" Read Data data """
arquivo = open("Au.txt", "r+")
texto = arquivo.readlines()
arquivo.close()
a = []
x = []
y = []

for i in texto:
    a.append(i.split(","))
for i in range(1,len(a)):
    x.append([])
    for j in range(0,len(a[i])):
        if(65 == j):
            y.append(float(a[i][j]))
        else:
            x[i-1].append(float(a[i][j]))

plt.hist(y, 50, normed=1, facecolor='green', alpha=0.75)
plt.xlabel('Quantidade de evadidos')
plt.ylabel('%')
plt.title("Histograma da Evasão")
plt.grid(True)
plt.show()
