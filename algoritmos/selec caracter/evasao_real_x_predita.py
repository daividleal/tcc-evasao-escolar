from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier
from sklearn.feature_selection import SelectFromModel
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import minmax_scale, scale
from sklearn.model_selection import RandomizedSearchCV, StratifiedKFold, KFold
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.optimizers import SGD
from keras.wrappers.scikit_learn import KerasRegressor
from keras.constraints import maxnorm
from sklearn.svm import SVR, NuSVR
import numpy as np
from sklearn.pipeline import Pipeline
from sklearn.feature_selection import SelectFromModel
import matplotlib.pyplot as plt
from mlxtend.feature_selection import SequentialFeatureSelector as SFS
from mlxtend.plotting import plot_sequential_feature_selection as plot_sfs
import matplotlib.pyplot as plt
from sklearn.model_selection import GridSearchCV
from mlxtend.plotting import plot_sequential_feature_selection as plot_sfs


""" Read Data data """
arquivo = open("Au.txt", "r+")
texto = arquivo.readlines()
arquivo.close()
a = []
x = []
y = []

for i in texto:
    a.append(i.split(","))
for i in range(1,len(a)):
    x.append([])
    for j in range(0,len(a[i])):
        if(65 == j):
            y.append(float(a[i][j]))
        else:
            x[i-1].append(float(a[i][j]))

x = np.array(x)
y = np.array(y)

eixo_x = []
for i in range(1, 342):
    eixo_x.append(i)
    
def createModel(neurons=17, activation='relu', init_mode='uniform', learn_rate=0.0012264789207924204, momentum=0.89659298182794145, dropout_rate=0.018702540495271529, weight_constraint=3, input_dim=x.shape[1]):
        """ Building network """
        model = Sequential()
        model.add(Dense(neurons, kernel_initializer=init_mode, activation=activation, input_dim=input_dim, kernel_constraint=maxnorm(weight_constraint)))
        model.add(Dropout(float(dropout_rate)))
        model.add(Dense(1, kernel_initializer=init_mode, activation='linear'))

        """ Training """
        sgd = SGD(lr=float(learn_rate), decay=1e-6, momentum=float(momentum), nesterov=True)
        model.compile(loss='mean_absolute_error',
                    optimizer=sgd,
                    metrics=['mae'])
        return model

""" Trained models """
svr = SVR()
svr.C = 0.2898087068229081
svr.degree = 4
svr.epsilon = 1.4006088388851361
svr.gamma = 0.11925439566117002
svr.kernel = 'linear'

nu_svr = NuSVR()
nu_svr.C = 3.7132193884820839
nu_svr.degree = 5
nu_svr.nu = 0.91892888835741759
nu_svr.gamma = 0.034429704202198173
nu_svr.kernel = 'sigmoid'

model = KerasRegressor(build_fn=createModel, epochs=392, batch_size=69, verbose=0)

""" ----------- """
estimatores = {}
# Prediction
estimator = GridSearchCV(model,dict(),scoring='neg_mean_absolute_error', cv=KFold(10))
estimator.fit(x, y)
estimatores["mlp"] = estimator

plt.scatter(y, estimator.predict(x), c="g", alpha=0.5,  label="Luck")
plt.ylabel("Valores preditos pela MLP")
plt.xlabel("Valores Reais")
plt.title('Resultados da SVM sem Seleção de características')
plt.show()

estimator = GridSearchCV(svr,dict(),scoring='neg_mean_absolute_error', cv=KFold(10))
estimator.fit(x, y)
estimatores["svr"] = estimator

plt.scatter(y, estimator.predict(x), c="g", alpha=0.5,  label="Luck")
plt.ylabel("Valores preditos pela SVR")
plt.xlabel("Valores Reais")
plt.title('Resultados da SVM sem Seleção de características')
plt.show()

estimator = GridSearchCV(nu_svr,dict(),scoring='neg_mean_absolute_error', cv=KFold(10))
estimator.fit(x, y)
estimatores["nu_svr"] = estimator

plt.scatter(y, estimator.predict(x), c="g", alpha=0.5,  label="Luck")
plt.ylabel("Valores preditos pela NU_SVR")
plt.xlabel("Valores Reais")
plt.title('Resultados da SVM sem Seleção de características')
plt.show()
