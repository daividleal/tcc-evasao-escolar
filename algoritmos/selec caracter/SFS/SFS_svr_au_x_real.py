from sklearn.model_selection import KFold
from sklearn.svm import SVR
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import GridSearchCV
from mlxtend.feature_selection import SequentialFeatureSelector as SFS
from mlxtend.plotting import plot_sequential_feature_selection as plot_sfs

""" Read Data data """
arquivo = open("Au.txt", "r+")
texto = arquivo.readlines()
arquivo.close()
a = []
x = []
y = []
X = []

for i in texto:
    a.append(i.split(","))

for i in range(1,len(a)):
    x.append([])
    X.append([])
    for j in range(0,len(a[i])):
        if(65 == j):
            y.append(float(a[i][j]))
        else:
            x[i-1].append(float(a[i][j]))

""" Trained model """
svr = SVR()
svr.C = 0.2898087068229081
svr.degree = 4
svr.epsilon = 1.4006088388851361
svr.gamma = 0.11925439566117002
svr.kernel = 'linear'

parametros_selecionados = [4, 7, 24, 25, 26, 27, 29, 31, 34, 37, 38, 40, 42, 43, 44, 50, 55, 56, 58, 59, 62]
for i in parametros_selecionados:
    for j in range(0,341):
        X[j].append(x[j][i])
        
X = np.array(X)
y = np.array(y)

estimator = GridSearchCV(svr, dict(), scoring='neg_mean_absolute_error', cv=KFold(10))
estimator.fit(X, y)

plt.scatter(y, estimator.predict(X), c="g", alpha=0.5,  label="Luck")
plt.ylabel("Valores preditos pela SVR")
plt.xlabel("Valores Reais")
plt.title('Resultados da SVM com Seleção de características')
plt.show()
