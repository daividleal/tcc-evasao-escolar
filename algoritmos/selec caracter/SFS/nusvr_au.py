from sklearn.model_selection import KFold
from sklearn.svm import NuSVR
import numpy as np
import matplotlib.pyplot as plt
from mlxtend.feature_selection import SequentialFeatureSelector as SFS
from mlxtend.plotting import plot_sequential_feature_selection as plot_sfs

""" Read Data data """
arquivo = open("Au.txt", "r+")
texto = arquivo.readlines()
arquivo.close()
a = []
x = []
y = []

for i in texto:
    a.append(i.split(","))

for i in range(1,len(a)):
    x.append([])
    for j in range(0,len(a[i])):
        if(65 == j):
            y.append(float(a[i][j]))
        else:
            x[i-1].append(float(a[i][j]))

""" Trained model """
nu_svr = NuSVR()
nu_svr.C = 3.7132193884820839
nu_svr.degree = 5
nu_svr.nu = 0.91892888835741759
nu_svr.gamma = 0.034429704202198173
nu_svr.kernel = 'sigmoid'

sfs1 = SFS(nu_svr,
           k_features=(1,65),
           forward=True,
           floating=True,
           verbose=1,
           scoring='neg_mean_absolute_error',
           cv=KFold(10))

sfs1 = sfs1.fit(np.array(x), y)
print('Selected features:', sfs1.k_feature_idx_)
fig1 = plot_sfs(sfs1.get_metric_dict(), kind='std_err')
plt.ylim([(sfs1.k_score_ - 0.5), (sfs1.k_score_ + 0.5)])
plt.ylabel("Cross Validation Score (MAE)")
plt.xlabel("Number of features selected")
plt.title('Sequential Forward Selection (w. std_err)')
plt.grid()
plt.show()