from sklearn.model_selection import KFold
from sklearn.svm import NuSVR
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import GridSearchCV
from mlxtend.feature_selection import SequentialFeatureSelector as SFS
from mlxtend.plotting import plot_sequential_feature_selection as plot_sfs

""" Read Data data """
arquivo = open("Au.txt", "r+")
texto = arquivo.readlines()
arquivo.close()
a = []
x = []
y = []
X = []

for i in texto:
    a.append(i.split(","))

for i in range(1,len(a)):
    x.append([])
    X.append([])
    for j in range(0,len(a[i])):
        if(65 == j):
            y.append(float(a[i][j]))
        else:
            x[i-1].append(float(a[i][j]))

""" Trained model """
nu_svr = NuSVR()
nu_svr.C = 3.7132193884820839
nu_svr.degree = 5
nu_svr.nu = 0.91892888835741759
nu_svr.gamma = 0.034429704202198173
nu_svr.kernel = 'sigmoid'

parametros_selecionados = [3, 7, 12, 14, 15, 17, 18, 20, 21, 25, 26, 27, 29, 31, 34, 37, 38, 39, 40, 43, 44, 49, 52, 53, 55, 56, 58, 59, 62]
for i in parametros_selecionados:
    for j in range(0,341):
        X[j].append(x[j][i])
        
X = np.array(X)
y = np.array(y)

estimator = GridSearchCV(nu_svr, dict(), scoring='neg_mean_absolute_error', cv=KFold(10))
estimator.fit(X, y)

plt.scatter(y, estimator.predict(X), c="g", alpha=0.5,  label="Luck")
plt.ylabel("Valores preditos pela NU_SVR")
plt.xlabel("Valores Reais")
plt.title('Resultados da SVM com Seleção de características')
plt.show()
