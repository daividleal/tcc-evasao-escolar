import matplotlib.pyplot as plt

""" Read Data data """
arquivo = open("Au.txt", "r+")
texto = arquivo.readlines()
arquivo.close()
a = []
x = []
y = []
X = []

for i in texto:
    a.append(i.split(","))

for i in range(1,len(a)):
    x.append([])
    X.append([])
    for j in range(0,len(a[i])):
        if(65 == j):
            y.append(float(a[i][j]))
        else:
            x[i-1].append(float(a[i][j]))

nomeFeatures = a[0]
mlp = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 13, 14, 16, 17, 18, 19, 21, 23, 25, 26, 27, 28, 29, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 43, 44, 45, 46, 47, 49, 50, 51, 52, 54, 55, 56, 58, 60, 61, 62, 63, 64]
svr = [4, 7, 24, 25, 26, 27, 29, 31, 34, 37, 38, 40, 42, 43, 44, 50, 55, 56, 58, 59, 62]
nusvr = [3, 7, 12, 14, 15, 17, 18, 20, 21, 25, 26, 27, 29, 31, 34, 37, 38, 39, 40, 43, 44, 49, 52, 53, 55, 56, 58, 59, 62]

intersection = set(mlp).intersection(svr).intersection(nusvr)
attr = [8,9,10,18,19,21,22,23,24,31,32,34,37,38,39,42,44,46,47,48,49,51,53,57,58,59,65]
escolhidos = set(attr).difference(intersection)
for i in intersection:
    valores_atributo = []
    binario = True
    for j in x:
        valor = j[i]
        valores_atributo.append(valor)
        if(valor > 0 and valor <1):
            binario = False

    plt.scatter(y, valores_atributo, c="g", alpha=0.5,  label="Luck")
    if(binario):
        plt.ylabel("Presença de " + nomeFeatures[i])
    else:
        plt.ylabel("Quantidade de " + nomeFeatures[i])
    plt.xlabel("Quantidade de alunos evadidos")
    plt.show()

##print("minhas_escolhas")
##
##for i in escolhidos:
##    valores_atributo = []
##    binario = True
##    for j in x:
##            valor = j[i-1]
##            valores_atributo.append(valor)
##            if(valor > 0 and valor <1):
##                binario = False
##
##    plt.scatter(y, valores_atributo, c="g", alpha=0.5,  label="Luck")
##    if(binario):
##        plt.ylabel("Presença de " + nomeFeatures[i-1])
##    else:
##        plt.ylabel("Quantidade de " + nomeFeatures[i-1])
##    plt.xlabel("Quantidade de alunos evadidos")
##    plt.show()
