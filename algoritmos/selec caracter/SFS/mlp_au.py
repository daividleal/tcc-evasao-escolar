import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='8'
from sklearn.model_selection import KFold
import scipy
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.optimizers import SGD
from keras.wrappers.scikit_learn import KerasRegressor
from keras.constraints import maxnorm
from sklearn.externals import joblib
from mlxtend.plotting import plot_sequential_feature_selection as plot_sfs
from sklearn import linear_model, decomposition, datasets
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
import matplotlib.pyplot as plt
import random

""" Read Data data """
arquivo = open("Au.txt", "r+")
texto = arquivo.readlines()
arquivo.close()
a = []
x = []
y = []
x1 = []

for i in texto:
    a.append(i.split(","))

for i in range(1,len(a)):
    x.append([])
    for j in range(0,len(a[i])):
        if(65 == j):
            y.append(float(a[i][j]))
        else:
            x[i-1].append(float(a[i][j]))

count = 0
for i in x:
    x1.append([])
    for j in i:
        x1[count].append(float(str(round(j,2))))
    count+=1

results = {}
mlp_dimension = random.sample(range(1,65), 20)
mlp_dimension.sort()
print(mlp_dimension)
for dim in mlp_dimension:
    resultado_dim = -10000
    for qtd in range(0,5):
        X = []
        for i in range(0,341):
            X.append([])
        b = random.sample(range(1,65), dim)
        b.sort()
        print(b)
        for i in b:
            for j in range(0, len(x1)):
                X[j].append(x1[j][i])

        def createModel(neurons=17, activation='relu', init_mode='uniform', learn_rate=0.0012264789207924204, momentum=0.89659298182794145, dropout_rate=0.018702540495271529, weight_constraint=3, input_dim=dim):
            """ Building network """
            model = Sequential()
            model.add(Dense(neurons, kernel_initializer=init_mode, activation=activation, input_dim=input_dim, kernel_constraint=maxnorm(weight_constraint)))
            model.add(Dropout(float(dropout_rate)))
            model.add(Dense(1, kernel_initializer=init_mode, activation='linear'))

            """ Training """
            sgd = SGD(lr=float(learn_rate), decay=1e-6, momentum=float(momentum), nesterov=True)
            model.compile(loss='mean_absolute_error',
                        optimizer=sgd,
                        metrics=['mae'])
            return model

        model = KerasRegressor(build_fn=createModel, epochs=392, batch_size=69, verbose=0)
        # Prediction
        estimator = GridSearchCV(model, dict(), scoring='neg_mean_absolute_error', cv=KFold(10))
        estimator.fit(X, y)

        if(resultado_dim < estimator.best_score_ ):
            results[dim] = {"MAE":estimator.best_score_, "Elementos": b}
    resultado_dim = -10000
    
""" Save Results """
##a = []
##for i in results:
##    a.append(str(i) + ":" + str(results[i]))
##
##arquivo = open("resultados_random_sfs_mlp.txt", "w+")
##arquivo.writelines(a)
##arquivo.close()
n_components = []
mae = []
best_score = -1000
components_best_score = []
number_components_best_score = 0
for i in results:
    n_components.append(i)
    score = results[i]["MAE"]
    mae.append(score)
    if(best_score < score):
        best_score = score
        components_best_score = results[i]["Elementos"]
        number_components_best_score = i

print(components_best_score)
plt.plot(n_components, mae, "bo", n_components, mae, "k")
plt.ylim([(best_score - 1), (best_score + 1)])
plt.ylabel("Cross Validation Score (MAE)")
plt.xlabel("Number of features selected")
plt.title('Resultados PCA')
plt.axvline(number_components_best_score,
            linestyle=':', label='Número de componentes Escolhidos')
plt.legend(prop=dict(size=12))
plt.grid()
plt.show()
