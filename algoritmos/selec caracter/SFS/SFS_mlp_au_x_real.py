import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='8'
from sklearn.model_selection import KFold
import scipy
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.optimizers import SGD
from keras.wrappers.scikit_learn import KerasRegressor
from keras.constraints import maxnorm
from sklearn.externals import joblib
from mlxtend.plotting import plot_sequential_feature_selection as plot_sfs
from sklearn import linear_model, decomposition, datasets
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
import matplotlib.pyplot as plt
import random

""" Read Data data """
arquivo = open("Au.txt", "r+")
texto = arquivo.readlines()
arquivo.close()
a = []
x = []
y = []
X = []

for i in texto:
    a.append(i.split(","))

for i in range(1,len(a)):
    x.append([])
    X.append([])
    for j in range(0,len(a[i])):
        if(65 == j):
            y.append(float(a[i][j]))
        else:
            x[i-1].append(float(a[i][j]))

parametros_selecionados = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 13, 14, 16, 17, 18, 19, 21, 23, 25, 26, 27, 28, 29, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 43, 44, 45, 46, 47, 49, 50, 51, 52, 54, 55, 56, 58, 60, 61, 62, 63, 64]
for i in parametros_selecionados:
    for j in range(0,341):
        X[j].append(x[j][i])
        
X = np.array(X)
y = np.array(y)

def createModel(neurons=17, activation='relu', init_mode='uniform', learn_rate=0.0012264789207924204, momentum=0.89659298182794145, dropout_rate=0.018702540495271529, weight_constraint=3, input_dim=len(parametros_selecionados)):
    """ Building network """
    model = Sequential()
    model.add(Dense(neurons, kernel_initializer=init_mode, activation=activation, input_dim=input_dim, kernel_constraint=maxnorm(weight_constraint)))
    model.add(Dropout(float(dropout_rate)))
    model.add(Dense(1, kernel_initializer=init_mode, activation='linear'))

    """ Training """
    sgd = SGD(lr=float(learn_rate), decay=1e-6, momentum=float(momentum), nesterov=True)
    model.compile(loss='mean_absolute_error',
                optimizer=sgd,
                metrics=['mae'])
    return model

model = KerasRegressor(build_fn=createModel, epochs=392, batch_size=69, verbose=0)
estimator = GridSearchCV(model, dict(), scoring='neg_mean_absolute_error', cv=KFold(10))
estimator.fit(X, y)

plt.scatter(y, estimator.predict(X), c="g", alpha=0.5,  label="Luck")
plt.ylabel("Valores preditos pela MLP")
plt.xlabel("Valores Reais")
plt.title('Resultados da MLP com Seleção de características')
plt.show()
