from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier
from sklearn.feature_selection import SelectFromModel
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import minmax_scale, scale
from sklearn.model_selection import RandomizedSearchCV, StratifiedKFold, KFold
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.optimizers import SGD
from keras.wrappers.scikit_learn import KerasRegressor
from keras.constraints import maxnorm
from sklearn.svm import SVR, NuSVR
import numpy as np
from sklearn.pipeline import Pipeline
from sklearn.feature_selection import SelectFromModel
import matplotlib.pyplot as plt
from mlxtend.feature_selection import SequentialFeatureSelector as SFS
from mlxtend.plotting import plot_sequential_feature_selection as plot_sfs
import matplotlib.pyplot as plt
from sklearn.model_selection import GridSearchCV
from mlxtend.plotting import plot_sequential_feature_selection as plot_sfs

""" Read Data data """
arquivo = open("Au.txt", "r+")
texto = arquivo.readlines()
arquivo.close()
a = []
x = []
y = []

for i in texto:
    a.append(i.split(","))

for i in range(1,len(a)):
    x.append([])
    for j in range(0,len(a[i])):
        if(65 == j):
            y.append(float(a[i][j]))
        else:
            x[i-1].append(float(a[i][j]))
            
""" Feature Selection """
clf = ExtraTreesClassifier()
clf = clf.fit(x, y)
base = SelectFromModel(clf, prefit=True)
X_new = base.transform(x)

def createModel(neurons=17, activation='relu', init_mode='uniform', learn_rate=0.0012264789207924204, momentum=0.89659298182794145, dropout_rate=0.018702540495271529, weight_constraint=3, input_dim=X_new.shape[1]):
        """ Building network """
        model = Sequential()
        model.add(Dense(neurons, kernel_initializer=init_mode, activation=activation, input_dim=input_dim, kernel_constraint=maxnorm(weight_constraint)))
        model.add(Dropout(float(dropout_rate)))
        model.add(Dense(1, kernel_initializer=init_mode, activation='linear'))

        """ Training """
        sgd = SGD(lr=float(learn_rate), decay=1e-6, momentum=float(momentum), nesterov=True)
        model.compile(loss='mean_absolute_error',
                    optimizer=sgd,
                    metrics=['mae'])
        return model


""" Trained models """
svr = SVR()
svr.C = 0.2898087068229081
svr.degree = 4
svr.epsilon = 1.4006088388851361
svr.gamma = 0.11925439566117002
svr.kernel = 'linear'

nu_svr = NuSVR()
nu_svr.C = 3.7132193884820839
nu_svr.degree = 5
nu_svr.nu = 0.91892888835741759
nu_svr.gamma = 0.034429704202198173
nu_svr.kernel = 'sigmoid'

model = KerasRegressor(build_fn=createModel, epochs=392, batch_size=69, verbose=0)

""" ----------- """
##scores = []
### Prediction
### Parameters of pipelines can be set using ‘__’ separated parameter names:
##estimator = GridSearchCV(model,dict(),scoring='neg_mean_absolute_error', cv=KFold(10))
##estimator.fit(X_new, y)
##scores.append(estimator.grid_scores_)
##
##estimator = GridSearchCV(svr,dict(),scoring='neg_mean_absolute_error', cv=KFold(10))
##estimator.fit(X_new, y)
##scores.append(estimator.grid_scores_)
##
##estimator = GridSearchCV(nu_svr,dict(),scoring='neg_mean_absolute_error', cv=KFold(10))
##estimator.fit(X_new, y)
##scores.append(estimator.grid_scores_)
##
##results = []
##for i in scores:
##    results.append(i[0][1]*-1)
##
##fig, ax = plt.subplots(1)
##ax.bar(["mlp", "svr", "nu_svr"], results, color=['g', '#624ea7', 'maroon'])
##results.sort()
##plt.ylim([0, results[0] + 1])
##plt.ylabel("Cross Validation Score (MAE)")
##plt.xlabel("Algoritmos")
##plt.title('Resultados Árvore')
##plt.show()
