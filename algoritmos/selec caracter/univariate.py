from sklearn.datasets import load_iris
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
import numpy as np

""" Read Data data """
arquivo = open("Au.txt", "r+")
texto = arquivo.readlines()
arquivo.close()
a = []
x = []
y = []

for i in texto:
    a.append(i.split(","))

for i in range(1,len(a)):
    x.append([])
    for j in range(0,len(a[i])):
        if(65 == j):
            y.append(float(a[i][j]))
        else:
            x[i-1].append(float(a[i][j]))
            

x = np.array(x)
y = np.array(y)

X_new = SelectKBest(chi2, k=2).fit_transform(x, y)
print(X_new.shape)
