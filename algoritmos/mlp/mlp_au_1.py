import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='8'
from sklearn.preprocessing import minmax_scale, scale
from sklearn.model_selection import RandomizedSearchCV
from scipy.stats import expon, uniform, randint
import scipy
from numpy import loadtxt
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.optimizers import SGD
from keras.wrappers.scikit_learn import KerasRegressor
from keras.constraints import maxnorm
from sklearn.externals import joblib

def createModel(neurons=1, activation='relu', init_mode='uniform', learn_rate=0.01, momentum=0.8, dropout_rate=0.5, weight_constraint=0, input_dim=65):
    """ Building network """
    model = Sequential()
    model.add(Dense(neurons, kernel_initializer=init_mode, activation=activation, input_dim=input_dim, kernel_constraint=maxnorm(weight_constraint)))
    model.add(Dropout(float(dropout_rate)))
    model.add(Dense(1, kernel_initializer=init_mode, activation='linear'))

    """ Training """
    sgd = SGD(lr=float(learn_rate), decay=1e-6, momentum=float(momentum), nesterov=True)
    model.compile(loss='mean_absolute_error',
                optimizer=sgd,
                metrics=['mae'])
    return model


""" fix random seed for reproducibility """
seed = 19
np.random.seed(seed)

""" Read Data data """
arquivo = open("Au.txt", "r+")
texto = arquivo.readlines()
arquivo.close()
a = []
x = []
y = []
x1 = []

for i in texto:
    a.append(i.split(","))

for i in range(1,len(a)):
    x.append([])
    for j in range(0,len(a[i])):
        if(65 == j):
            y.append(float(a[i][j]))
        else:
            x[i-1].append(float(a[i][j]))

count = 0
for i in x:
    x1.append([])
    for j in i:
        x1[count].append(float(str(round(j,2))))
    count+=1

## -------------------------------------------------------------------
##""" Pre-processing data """
##x=loadtxt('Au.txt')
##y = x[:,-1]
##x = x[:,0:-1]
##minmax_scale(x,copy=False)
# Don't need this anymore, The target function is already a percentage
# scale(y,copy=False)
## -------------------------------------------------------------------

""" Create model """
model = KerasRegressor(build_fn=createModel, epochs=100, batch_size=30, verbose=0)
param_dist = {'batch_size': randint(low=1,high=100), 'epochs': randint(low=1,high=1000), 'activation': ['softmax', 'softplus', 'softsign', 'relu', 'tanh', 'sigmoid', 'hard_sigmoid', 'linear'], 'init_mode': ['uniform', 'lecun_uniform', 'normal', 'zero', 'glorot_normal', 'glorot_uniform', 'he_normal', 'he_uniform'], 'learn_rate': uniform(loc=1e-10,scale=0.3), 'momentum': uniform(loc=0.5,scale=0.5), 'weight_constraint': randint(low=1,high=6), 'dropout_rate': uniform(loc=1e-10,scale=1), 'neurons': randint(low=1,high=100)}
scoring = {'mse': 'neg_mean_squared_error', 'mae': 'neg_mean_absolute_error', 'r2':'r2', 'ev':'explained_variance'}
n_iter_search=50
grid = RandomizedSearchCV(estimator=model, param_distributions=param_dist, n_iter=n_iter_search, n_jobs=8, scoring=scoring, refit='mae', cv=10, verbose=1)
grid_result = grid.fit(x1, y)

a = []
""" summarize results """
a.append("----------------------------------------------------------" + "\n")
a.append("----------------------------------------------------------" + "\n")
a.append("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_) + "\n")
a.append("----------------------------------------------------------" + "\n")
means_mae = grid_result.cv_results_['mean_test_mae']
stds_mae = grid_result.cv_results_['std_test_mae']
means_mse = grid_result.cv_results_['mean_test_mse']
stds_mse = grid_result.cv_results_['std_test_mse']
means_r2 = grid_result.cv_results_['mean_test_r2']
stds_r2 = grid_result.cv_results_['std_test_r2']
means_ev = grid_result.cv_results_['mean_test_ev']
stds_ev = grid_result.cv_results_['std_test_ev']
params = grid_result.cv_results_['params']
for mean_mae, stdev_mae, mean_mse, stdev_mse, mean_r2, stdev_r2, mean_ev, stdev_ev, param in zip(means_mae, stds_mae,means_mse, stds_mse, means_r2, stds_r2,means_ev, stds_ev,params):
    a.append("mean mae=%f (std=%f) with: %r" % (mean_mae, stdev_mae, param) + "\n")
    a.append("mean mse=%f (std=%f) with: %r" % (mean_mse, stdev_mse, param) + "\n")
    a.append("mean r2=%f (std=%f) with: %r" % (mean_r2, stdev_r2, param) + "\n")
    a.append("mean ev=%f (std=%f) with: %r" % (mean_ev, stdev_ev, param) + "\n")
    a.append("****************************************************" + "\n")

arquivo = open("restauldo_mlp_19.txt", "w+")
arquivo.writelines(a)
arquivo.close()
##grid.best_estimator_.model.save('results_mlp_au.h5')

"""
joblib.dump(grid.best_estimator_, 'Au5.pkl', compress = 1)

print("----------------- Other results -------------------")
# validator.best_estimator_ returns sklearn-wrapped version of best model.
# validator.best_estimator_.model returns the (unwrapped) keras model
best_model = grid.best_estimator_.model
metric_names = best_model.metrics_names
metric_values = best_model.evaluate(x_test, y_test)
for metric, value in zip(metric_names, metric_values):
    print(metric, ': ', value)
"""
